/*
 * keypad.c
 *
 * Created: 6/19/2020 3:58:10 PM
 * Author : Ali-Hejazi
 */ 


#define F_CPU 8000000
#include <avr/io.h>
#include <util/delay.h>

unsigned char k[4][3]={{0xF9,0xA4,0xB0},{0x99,0x92,0x82},{0xF8,0x80,0x90},{0x88,0xc0,0xc0}};


int main(void)
{
    /* Replace with your application code */
	unsigned char c,r;
	DDRD = 0XFF;
	DDRC = 0XFF;
	DDRA=0xf0;
	PORTA=0xff;
	
	PORTD = 0xc0;
	PORTC = 0xc0;
	while(1)
	{
		
		do
		{
			PORTA&=0x0f;
			r=PINA&0x0f;
		} while(r!=0x0f);
		

		do
		{
			_delay_ms(20);
			r=PINA&0x0f;
		}	  while(r==0x0f);
		
		while(1)
		{
			PORTA=0xef;
			r=PINA&0x0f;
			if(r!=0x0f)
			{
				c=0;
				break;
			}
			PORTA=0xdf;
			r=PINA&0x0f;
			if(r!=0x0f)
			{
				c=1;
				break;
			}
			PORTA=0xbf;
			r=PINA&0x0f;
			if(r!=0x0f)
			{
				c=2;
				break;
			}

		}

		if(r==0x0e)
		{
		PORTC = PORTD;
		PORTD=k[0][c];
		}
		else if(r==0x0d)
		{
		PORTC = PORTD;
		PORTD=k[1][c];
		}
		else if(r==0x0b)
		{
		PORTC = PORTD;
		PORTD=k[2][c];
		}
		else
		{
		PORTC = PORTD;
		PORTD=k[3][c];
		}
	}
	return 0;
}

